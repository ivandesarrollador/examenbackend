<?php

class BaseDatos{

 // creamos los atributos de la Clase correspondiente los cueles nose serviran para poder realizar la conexion con la db.
 private $host = "localhost";
 // el usuario por este momento es root pero, no es recomendable dejar el perfil como root en un proyecto más grande donde participan más programadores
 private $usuario = "root";
 private $password = "hackerANTRAX";
 private $base = "prueba";

 // metódo el cual me permite conectarme a la BD
 public function conectar(){
    $conexion_mysql = "mysql:host=$this->host;dbname=$this->base";
            $conexionDB = new PDO($conexion_mysql, $this->usuario, $this->password);
            $conexionDB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            //Esta linea arregla la codificacion sino no aparecen en la salida en JSON quedan NULL
            $conexionDB -> exec("set names utf8");
            return $conexionDB;
 }

}

?>
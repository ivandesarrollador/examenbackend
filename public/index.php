<?php
// cabeceras necesarias para hacer las peticiones mediante http o https
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");

// Yo utilice SLIm el cual es un framework de PHP para la creación de micro servicios Rest
//Importaciones necesarias de SLIM
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
//Importaciones necesarias de SLIM
require '../vendor/autoload.php';
require '../datos/conexion.php';

$app = new \Slim\App;
// ejemplo de prueba solo para probar si Slim esta funcionado perfectamente
$app->get('/hello/{name}', function (Request $request, Response $response, array $args) {
    $name = $args['name'];
    $response->getBody()->write("Hello, $name");

    return $response;
});

// agregamos las rutas

require "../src/rutas/clientes.php";
// corremos la aplicación SLIm
$app->run();

?>
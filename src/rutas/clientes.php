<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// instanciamos un objeto de tipo Slim para poder utilizar el framework
$app = new \Slim\App;

// este es le método el cual permite agregar un nuevo cliente a la BD
$app->post('/api/addCliente',function(Request $request,Response $response){

    // obtengo cada uno de los parametros correspondientes por medio de el Request de la Función
    $nombre = $request->getParam('nombre');
    $edad = $request->getParam('edad');
    $email = $request->getParam('email');
    $estatus = $request->getParam('estatus');

// verifico si los parametros son nulos, si lo son no permite insertar en la db
    if(is_null($nombre)  && is_null($edad) && is_null($estatus)){
        $json = array("status" => 0, "message" => "Faltan campos");

    }
    else{
        
        //creo la consulta y despues la ejecuto 
        $consulta = "INSERT INTO clientes (nombre, edad, email, estatus) VALUES
       ('$nombre', $edad,'$email',$estatus)";
        try{
           // instanciamos un objeto de tipo BaseDatos para poder acceder a sus métodos como conectar etc.
            $db = new  BaseDatos();
            // Conexión
            $db = $db->conectar();
            $ejecutar = $db->query($consulta);
            $json = array("status" => 1, "message" => "Cliente agregado");
            // asignamos valor de null a nuestro objeto para cerrar caulquier conección a la db.
            $db = null;
           
        
        } catch(PDOException $e){
            $json = array("status"=> 2, "message"=> $e->getMessage());
        }
       
    }
    // combierto al array a json para ser enviado como Response
     echo json_encode($json);
});


// método el cual me permite listar todos los usuarios registrados 
$app->get('/api/listClientes',function(Request $request,Response $response){
   
    // creo un array vacio y estructuro mi consulta. 
    $userData = array();
    $consulta = "SELECT * FROM clientes";
    try
    {
          // instanciamos un objeto de tipo BaseDatos para poder acceder a sus métodos como conectar etc.
        $db = new BaseDatos();
        $db = $db->conectar();
        $ejecutar = $db->query($consulta);
        $clientes = $ejecutar->fetchAll(PDO::FETCH_OBJ);
         // asignamos valor de null a nuestro objeto para cerrar caulquier conección a la db.
        $db = null;
     $json = $clientes;
    
    } catch(PDOException $e){
        $json = array("error:"=> $e->getMessage());
    }
    // combierto al array a json para ser enviado como Response
    echo json_encode($json);
});

// método el cual permite poder editar la información del cliente.
$app->post('/api/editCliente',function(Request $request,Response $response){
    // obtengo cada uno de los parametros correspondientes por medio de el Request de la Función
    $id = $request->getParam('idCliente');
    $nombre = $request->getParam('nombre');
    $edad = $request->getParam('edad');
    $email = $request->getParam('email');
    $estatus = $request->getParam('estatus');

    // verifico si los parametros son nulos, si lo son no permite insertar en la db
    if(is_null($id) && is_null($nombre)  && is_null($edad) && is_null($estatus)){
        $json = array("status" => 0, "message" => "Faltan campos");
    }
    else{
         // estructuro mi consulta para actualizar
        $consulta = "UPDATE  clientes SET nombre ='$nombre',edad = $edad, email ='$email',estatus =$estatus WHERE idCliente = $id";
        try{
             // instanciamos un objeto de tipo BaseDatos para poder acceder a sus métodos como conectar etc.
            $db = new  BaseDatos();
            // Conexión
            $db = $db->conectar();
            $ejecutar = $db->query($consulta);
        
            $json = array("status" => 1, "message" => "Cliente actualizado");
             // asignamos valor de null a nuestro objeto para cerrar caulquier conección a la db.
            $db = null;
           
        
        } catch(PDOException $e){
            $json = array("status"=> 2, "message"=> $e->getMessage());
        }
       
    }
      // combierto al array a json para ser enviado como Response
     echo json_encode($json);
});

// método el cual permite  eliminar al cliente de la tabla
$app->post('/api/deleteCliente',function(Request $request,Response $response){
    $id = $request->getParam('idCliente');
   
    if(is_null($id)){
        $json = array("status" => 0, "message" => "Faltan campos");
    }
    else{

        $consulta = "DELETE FROM clientes WHERE idCliente = $id";
        try{
             // instanciamos un objeto de tipo BaseDatos para poder acceder a sus métodos como conectar etc.
            $db = new  BaseDatos();
            // Conexión
            $db = $db->conectar();
            $ejecutar = $db->query($consulta);

            $json = array("status" => 1, "message" => "Cliente Eliminado");
             // asignamos valor de null a nuestro objeto para cerrar caulquier conección a la db.
            $db = null;
           
        
        } catch(PDOException $e){
            $json = array("status"=> 2, "message"=> $e->getMessage());
        }
    }
         // combierto al array a json para ser enviado como Response
     echo json_encode($json);
});

// método el cual permite actualizar el estatus del cliente
$app->post('/api/setClienteEstatus',function(Request $request,Response $response){
    $id = $request->getParam('idCliente');
    $estatus = $request->getParam('estatus');
   
    if(is_null($id) && is_null($estatus)){
        $json = array("status" => 0, "message" => "Faltan campos");

    }
    else{

        $consulta = "UPDATE clientes SET estatus = $estatus WHERE idCliente = $id";
        try{
             // instanciamos un objeto de tipo BaseDatos para poder acceder a sus métodos como conectar etc.
            $db = new  BaseDatos();
            // Conexión
            $db = $db->conectar();
            $ejecutar = $db->query($consulta);
            $json = array("status" => 1, "message" => "Estatus Actualizado");
             // asignamos valor de null a nuestro objeto para cerrar caulquier conección a la db.
            $db = null;
           
        } catch(PDOException $e){
            $json = array("status"=> 2, "message"=> $e->getMessage());
        }
    }
         // combierto al array a json para ser enviado como Response
     echo json_encode($json);
});

?>